# Инструкция по применению

## Перед запуском

Создать .env файл

В него указать переменные:
- BOT_TOKEN - токен от botFather;
- WEB_APP_URL - ссылка на web app.

```
BOT_TOKEN="EXAMPLE"
WEB_APP_URL="EXAMPLE"
```

## Установка зависимостей

```
npm i
```

## Запуска бота

```
npm run start
```
