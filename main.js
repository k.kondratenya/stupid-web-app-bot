const { Telegraf, web_app } = require('telegraf')
require('dotenv').config();

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.start((ctx) => {
	const defaultButton = {
		text: 'Введите данные',
		web_app: {
			url: process.env.WEB_APP_URL
		}
	}
	const print = 'Welcome'
	ctx.reply(print, {
		reply_markup: JSON.stringify({
			resize_keyboard: true,
			keyboard: [
				[ defaultButton ],
			]
		})
	})
})

bot.on('web_app_data', (ctx) => {
	const print =  `Данные формы ${ctx.message.web_app_data.data}`
	ctx.reply(print, {
		reply_markup: JSON.stringify({
			remove_keyboard: true
		}),
	})
})

bot.launch()

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))
